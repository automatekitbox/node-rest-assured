"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const jsonValidationUtils_1 = require("../../src/common-helpers/rest-helper/jsonValidationUtils");
describe("jsonValidationUtils", () => {
    describe("prettyPrintJSON", () => {
        it("should return formatted JSON", async () => {
            const data = {
                testProp: "testValue"
            };
            const json = JSON.stringify(data);
            chai_1.expect(json.indexOf("\n")).to.be.lessThan(0);
            const formattedJson = jsonValidationUtils_1.prettyPrintJSON(data);
            chai_1.expect(formattedJson.indexOf("\n")).to.be.greaterThan(0);
            chai_1.expect(formattedJson).to.equal("{\n \"testProp\": \"testValue\"\n}");
        });
    });
});
//# sourceMappingURL=jsonValidationUtils.spec.js.map