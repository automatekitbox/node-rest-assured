// This fetch polyfill was adapted from fetch-npm-node.js in github.com/matthew-andrews/isomorphic-fetch V2.2.1.
// That package/version tries to overwrite the global.Response, global.Headers and global.Request, which are read-only.
// I'm not sure if this is related to a change in node (V7.4.0), or something else, but this polyfill appears to address
// the issue of overwriting the read-only properties.
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:no-var-requires
const realFetch = require("node-fetch");
// tslint:disable-next-line:ban-types
const newFetch = (url, options) => {
    if (/^\/\//.test(url)) {
        url = "https:" + url;
    }
    return realFetch.call(this, url, options);
};
exports.default = newFetch;
if (!global.fetch) {
    global.fetch = newFetch;
    /* These are read-only, so cannot be reassigned.
    global.Response = realFetch.Response;
    global.Headers = realFetch.Headers;
    global.Request = realFetch.Request;
    */
}
//# sourceMappingURL=node-fetch-polyfill.js.map