declare const newFetch: (url: string, options: Object) => any;
export default newFetch;
