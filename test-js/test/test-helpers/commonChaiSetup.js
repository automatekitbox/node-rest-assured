"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// Import and configure chai
//
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const sinonChai = require("sinon-chai");
chai.use(chaiAsPromised);
chai.use(sinonChai);
chai.config.includeStack = true;
//# sourceMappingURL=commonChaiSetup.js.map