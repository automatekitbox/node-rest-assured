"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
const scp_component_logging_1 = require("@aptos-scp/scp-component-logging");
//
// Initialize Logging
//
// Initialize the LogManager logging level to either the value specified by environment variable LOG_LEVEL or,
// if invalid or not set, to a default value.
//
// This allows us to run coverage tests with a log level of ALL to ensure maximum coverage AND to ensure
// that all log statements can actually execute.
//
const DEFAULT_LOG_LEVEL = scp_component_logging_1.LogLevel.INFO;
let LOG_LEVEL = scp_component_logging_1.LOG_LEVEL_LABELS.indexOf(process.env.LOG_LEVEL);
if (LOG_LEVEL < 0) {
    LOG_LEVEL = DEFAULT_LOG_LEVEL;
    // tslint:disable-next-line:no-console
    console.info(`Using default log level ${scp_component_logging_1.LOG_LEVEL_LABELS[LOG_LEVEL]} (${LOG_LEVEL})`);
}
else {
    // tslint:disable-next-line:no-console
    console.info(`Using log level from environment: ${process.env.LOG_LEVEL} (${LOG_LEVEL})`);
}
// Set log level for testing
scp_component_logging_1.LogManager.setFilterLevel(LOG_LEVEL);
//# sourceMappingURL=initializeLogging.js.map