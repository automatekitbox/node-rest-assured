import { TableDefinition } from "cucumber";
/**
 * This method is used to display JSON response in an easy-to-read format
 * @param {object} json - JSON response
 */
export declare function prettyPrintJSON(json: object): string;
/**
 * This method is used for the validation of JSON response after making http request.
 * Where sending input as JSON Response and DataTable
 * Constructing map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validJsonPathWithRegularExpressionForMap: It validates JSON response with DataTable values
 * @param {object} responseBody - json response body
 * @param {TableDefinition} responseFields - reading datatable from feature file
 * @returns {void}
 */
export declare function validateJsonPathWithRegularExpression(responseBody: object, responseFields: TableDefinition): void;
/**
 * This method is used for the negative validation of JSON response after making http request
 * Json response should not contain json parameter values that are passed from feature file (DataTable)
 * Where sending input as JSON Response and DataTable
 * Where DataTable is 2D array , sending row and colums as key value pair
 * This will internally refer invalidJsonPathWithRegularExpressionForMap by passing JSON path(reponseBody)
 * invalidJsonPathWithRegularExpressionForMap: It validates JSON response with DataTable values
 * @param {object} responseBody - json response
 * @param {TableDefinition} responseFields - DataTable from feature file
 * @returns {void}
 */
export declare function invalidJsonPathWithRegularExpression(responseBody: object, responseFields: TableDefinition): void;
/**
 * This method is used for the validation of JSON response after making http request.
 * Where sending input as JSON Response and DataTable as Map
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validJsonPathWithRegularExpressionForMap: It validates JSON response with Map values
 * @param {object} responseBody - response body
 * @param {Map<string, string>} responsemap - data table fields in Map
 * @returns {void}
 */
export declare function validJsonPathWithRegularExpressionForMap(responseBody: object, responsemap: Map<string, string>): void;
/**
 * This method is used for the negative validation of JSON response after making http request
 * Json response should not contain json parameter(node) values that are passed from feature file (DataTable)
 * Where sending input as JSON Response and DataTable as Map
 * Where DataTable is 2D array , sending row and colums as key value pair
 * invalidJsonPathWithRegularExpressionForMap: It validates JSON response with Map values
 * @param {object} responseBody - response body
 * @param {Map<string, string>} datableFieldsInMap - data table fields in Map
 * @returns {void}
 */
export declare function invalidJsonPathWithRegularExpressionForMap(responseBody: object, datableFieldsInMap: Map<string, string>): void;
/**
 * This method is used to get parent node JSON value by replacing JSON path with the given parent node
 * @param {object} responseBody - response body
 * @param {string} parentnode - parent node
 * @param {string} value - value
 * @returns {string} - JSON path
 */
export declare function replaceJsonPathFollowedByParentIndex(responseBody: object, parentnode: string, value: string): string;
/**
 * This method is used for validating JSON response followed by parent node index after making http request
 * Where sending input as JSON Response and DataTable
 * Where DataTable is 2D array , sending row and colums as key value pair
 * Constructing Map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validateJsonMapFollowedByParentIndex: It validates map values with JSON response followed by parent node
 * followed by responseBody JSON path and parent index.
 * @param {string} parentNode - parent node
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - reponse fields
 * @returns {void}
 */
export declare function validateJsonDataTableFollowedByParentIndex(parentNode: string, responseBody: object, responseFields: TableDefinition): void;
/**
 * This method is used for validating JSON response followed by parent node index after making http request
 * Where sending input as JSON Response and DataTable as Map
 * Where DataTable is 2D array , sending row and colums as key value pair
 * Constructing Map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validateJsonMapFollowedByParentIndex: It validates map values with JSON response followed by parent node
 * followed by responseBody JSON path and parent index.
 * @param {string} parentKey - parent key
 * @param {object} responseBody - response body
 * @param {Map<string, string>} responseMap - response map
 * @returns {void}
 */
export declare function validateJsonMapFollowedByParentIndex(parentKey: string, responseBody: object, responseMap: Map<string, string>): void;
/**
 * This method is used to read the JSON file from the path specified
 * @param {string} jsonfilepath - JSON file path
 */
export declare function readJson(jsonfilepath: string): Promise<object>;
/**
 * This method is used to read the JSON file from the specified path to create a JSON object
 * @param {string} jsonfilepath - JSON file path
 */
export declare function readJsonToObject(jsonfilepath: string): Promise<object>;
/**
 * This method is used to read the JSON file from the specified path to create a JSON string
 * @param {string} jsonfilepath - JSON file path
 */
export declare function readJsonToString(jsonfilepath: string): Promise<string>;
/**
 * This method is used to find the parent node by name
 * @param {object} jsonData - JSON data
 * @param {string} nodename - name of the node
 */
export declare function findNodeByName(jsonData: object, nodename: string): Promise<string[]>;
/**
 * This method is used to POST request parameters from the DataTable that are specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {TableDefinition} responseFields - reponse fields
 */
export declare function postRequestParametersFromDataTable(jsonfilepath: string, responseFields: TableDefinition): Promise<string>;
/**
 * This method is used to POST request parameters from the DataTable as Map specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 */
export declare function postRequestParametersFromMap(jsonfilepath: string, dataTableFieldsInMap: Map<string, string>): Promise<string>;
/**
 * This method is used to find the parent node value by node name
 * @param {object} jsonData - JSON data
 * @param {string} nodename - name of the node
 */
export declare function findNodeValue(jsonData: object, nodename: string): Promise<string[]>;
/**
 * This method is used for validation of JSON response after making http request by giving JSON path
 * returns the table as a 2-D array, without the first row
 * where first row from datatable using it as column headers.
 * validJsonPathWithRegularExpressionForMap: It validates JSON response passed as key and value pair
 * after making service(REST) call, where key is JSON node(path) name and value is JSON node(path)
 * value with the responseBody JSON path.
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - response fields
 * @returns {void}
 */
export declare function excludeFirstRowFromTableAndValidateJsonPath(responseBody: object, responseFields: TableDefinition): void;
/**
 * This method is used for validating JSON response followed by parent index
 * returns the data table as a 2-D array, without the first row
 * where first row from datatable using it as column headers.
 * validateJsonMapFollowedByParentIndex:It validates JSON response with Map as key and value pair
 * after making service(REST) call, where key is JSON node(path) name and value is JSON node(path) value
 * followed by responseBody JSON path and parent index.
 * @param {string} parentNode - parent node
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - response fields
 * @returns {void}
 */
export declare function excludeFirstRowAndValidateJsonPathFollowedByParentIndex(parentNode: string, responseBody: object, responseFields: TableDefinition): void;
/**
 * This method is used to POST request parameters from the DataTable that are specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {TableDefinition} responseFields - reponse fields
 */
export declare function excludeHeadersAndPostRequestParametersFromDataTable(jsonfilepath: string, responseFields: TableDefinition): Promise<string>;
/**
 * This method is used to read response file in json format as well parameters from the DataTable as Map.
 * Where values of Map needs to be replaced in the input json file object
 * Where input response json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 * @returns {Promise<object>} - Returns Json object
 */
export declare function createJsonResponseObjectFromMap(jsonResponsefilepath: string, dataTableFieldsInMap: Map<string, string>): Promise<object>;
/**
 * This method is used to validate json response objects
 * These input json response file converting as json object.
 * Where internally calls deepCompareOfJsonObjects method to validate two json objects
 * @param {string} jsonfilepath - JSON Response as Input File
 * @param {object} actualJsonResponseObject - Getting json response after making REST call
 * @returns {Promise<void>} - returns nothing
 */
export declare function validateJsonResponseFile(jsonResponsefilepath: string, actualJsonResponseObject: object): Promise<void>;
/**
 * This method is used to POST request parameters as json file
 * These input json file converting as json object.
 * Where JsonObject converting to JSon string
 * @param {string} jsonfilepath - JSON file path
 * @returns {Promise<String>} - returns json string
 */
export declare function postRequestParametersAsFile(jsonfilepath: string): Promise<string>;
/**
 * This method is used to compare of two json objects by every Key
 * Which gives exactly Key that fails for particular test
 * Where passing two json objects as inputs
 * @param {object} expectedJsonObject - First json object
 * @param {object} actualJosnObject - second json object that needs to be compared
 * @returns {<void>} - Returns Json object
 */
export declare function deepCompareOfJsonObjects(expectedJsonObject: object, actualJosnObject: object): void;
/**
 * This method is used to POST request parameters from the DataTable as Map specified in the input json
 * These requested parameters will be applied on input json object.
 * Same json object will be used as input for post request
 * @param {object} jsonObject - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 */
export declare function postJsonObjectRequestFromMap(jsonObject: object, dataTableFieldsInMap: Map<string, string>): Promise<string>;
