"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const jsonPath = require("jsonpath");
const load_json_file_1 = require("load-json-file");
const log4js = require("log4js");
const logger = log4js.getLogger();
logger.level = "debug";
/**
 * This method is used to display JSON response in an easy-to-read format
 * @param {object} json - JSON response
 */
function prettyPrintJSON(json) {
    return JSON.stringify(json, null, " ");
}
exports.prettyPrintJSON = prettyPrintJSON;
/**
 * This method is used for the validation of JSON response after making http request.
 * Where sending input as JSON Response and DataTable
 * Constructing map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validJsonPathWithRegularExpressionForMap: It validates JSON response with DataTable values
 * @param {object} responseBody - json response body
 * @param {TableDefinition} responseFields - reading datatable from feature file
 * @returns {void}
 */
function validateJsonPathWithRegularExpression(responseBody, responseFields
/*reading column values from feature file */ ) {
    logger.debug("Entry validateJsonPathWithRegularExpression");
    const readFeatureDataTable = responseFields.rowsHash();
    const responsemap = new Map();
    for (const key in readFeatureDataTable) {
        if (readFeatureDataTable.hasOwnProperty(key)) {
            logger.debug(`Key is: ${key} Value is: ${readFeatureDataTable[key]}`);
            responsemap.set(key, readFeatureDataTable[key]);
        }
    }
    validJsonPathWithRegularExpressionForMap(responseBody, responsemap);
    logger.debug("Exit validateJsonPathWithRegularExpression");
}
exports.validateJsonPathWithRegularExpression = validateJsonPathWithRegularExpression;
/**
 * This method is used for the negative validation of JSON response after making http request
 * Json response should not contain json parameter values that are passed from feature file (DataTable)
 * Where sending input as JSON Response and DataTable
 * Where DataTable is 2D array , sending row and colums as key value pair
 * This will internally refer invalidJsonPathWithRegularExpressionForMap by passing JSON path(reponseBody)
 * invalidJsonPathWithRegularExpressionForMap: It validates JSON response with DataTable values
 * @param {object} responseBody - json response
 * @param {TableDefinition} responseFields - DataTable from feature file
 * @returns {void}
 */
function invalidJsonPathWithRegularExpression(responseBody, responseFields) {
    const readFeatureDataTable = responseFields.rowsHash();
    const responsemap = new Map();
    for (const key in readFeatureDataTable) {
        if (readFeatureDataTable.hasOwnProperty(key)) {
            logger.debug(() => `Key is: ${key} Value is: ${readFeatureDataTable[key]}`);
            responsemap.set(key, readFeatureDataTable[key]);
        }
    }
    invalidJsonPathWithRegularExpressionForMap(responseBody, responsemap);
}
exports.invalidJsonPathWithRegularExpression = invalidJsonPathWithRegularExpression;
/**
 * This method is used for the validation of JSON response after making http request.
 * Where sending input as JSON Response and DataTable as Map
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validJsonPathWithRegularExpressionForMap: It validates JSON response with Map values
 * @param {object} responseBody - response body
 * @param {Map<string, string>} responsemap - data table fields in Map
 * @returns {void}
 */
function validJsonPathWithRegularExpressionForMap(responseBody, responsemap) {
    for (const [actualPath, expectedValue] of responsemap) {
        const queryResult = jsonPath.query(responseBody, "$.." + actualPath);
        logger.debug(() => `Json query result is: ${queryResult}`);
        logger.debug(() => `Json expected value is json query result is: ${expectedValue}`);
        if (expectedValue.toString().includes(";")) {
            expectedValue.toString().split(";").forEach((value) => {
                chai_1.assert(queryResult.some((val) => val === value), `${actualPath} or ${value} is not present in response`);
            });
        }
        else {
            chai_1.expect(queryResult.some((x) => x.toString() === expectedValue.toString()), `${queryResult} is not equals to ${expectedValue} OR ${actualPath} is not present in response`)
                .to.be.true;
            logger.info(() => `${actualPath} or ${expectedValue} is present in response`);
        }
    }
}
exports.validJsonPathWithRegularExpressionForMap = validJsonPathWithRegularExpressionForMap;
/**
 * This method is used for the negative validation of JSON response after making http request
 * Json response should not contain json parameter(node) values that are passed from feature file (DataTable)
 * Where sending input as JSON Response and DataTable as Map
 * Where DataTable is 2D array , sending row and colums as key value pair
 * invalidJsonPathWithRegularExpressionForMap: It validates JSON response with Map values
 * @param {object} responseBody - response body
 * @param {Map<string, string>} datableFieldsInMap - data table fields in Map
 * @returns {void}
 */
function invalidJsonPathWithRegularExpressionForMap(responseBody, datableFieldsInMap) {
    for (const [actualPath, expectedValue] of datableFieldsInMap) {
        const queryResult = jsonPath.query(responseBody, "$.." + actualPath);
        logger.info(() => `Json query result is: ${queryResult}`);
        if (queryResult.length === 0) {
            chai_1.expect(queryResult, `${queryResult} equals to ${expectedValue} OR ${actualPath} is present in response`)
                .to.be.empty;
            logger.info(() => `${actualPath} is not present in response`);
        }
        else if (queryResult.toString().includes(",")) {
            const queryResultValue = queryResult.toString().split(",");
            chai_1.expect(queryResultValue.indexOf(expectedValue) === -1, `${expectedValue} is present in actual response ${queryResult}`).to.be.true;
            logger.info(() => `${actualPath} or ${expectedValue} is not present in response`);
        }
        else {
            chai_1.expect(queryResult.some((x) => x.toString() !== expectedValue.toString()), `${queryResult} is equals to ${expectedValue} OR ${actualPath} is present in response`)
                .to.be.true;
            logger.info(() => `${actualPath} or ${expectedValue} is not present in response`);
        }
    }
}
exports.invalidJsonPathWithRegularExpressionForMap = invalidJsonPathWithRegularExpressionForMap;
/**
 * This method is used to get parent node JSON value by replacing JSON path with the given parent node
 * @param {object} responseBody - response body
 * @param {string} parentnode - parent node
 * @param {string} value - value
 * @returns {string} - JSON path
 */
function replaceJsonPathFollowedByParentIndex(responseBody, parentnode, value) {
    let parentNode = "";
    const paths = jsonPath.paths(responseBody, "$.." + parentnode);
    for (const index in paths) {
        if (paths.hasOwnProperty(index)) {
            const jsonpath = jsonPath.stringify(paths[index]);
            const patharray = jsonpath.split(".");
            const jsonvalue = jsonPath.value(responseBody, jsonpath.replace("$", "$."));
            logger.debug(() => `Parent node Json value is: ${jsonvalue}`);
            if (jsonvalue != null && jsonvalue.toString() === value.toString()) {
                parentNode = patharray[patharray.length - 2];
                logger.debug(() => `Parent node is: ${parentNode}`);
                return parentNode;
            }
        }
    }
    return parentNode;
}
exports.replaceJsonPathFollowedByParentIndex = replaceJsonPathFollowedByParentIndex;
/**
 * This method is used for validating JSON response followed by parent node index after making http request
 * Where sending input as JSON Response and DataTable
 * Where DataTable is 2D array , sending row and colums as key value pair
 * Constructing Map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validateJsonMapFollowedByParentIndex: It validates map values with JSON response followed by parent node
 * followed by responseBody JSON path and parent index.
 * @param {string} parentNode - parent node
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - reponse fields
 * @returns {void}
 */
function validateJsonDataTableFollowedByParentIndex(parentNode, responseBody, responseFields /*reading column values
from feature file */) {
    const readFeatureDataTable = responseFields.rowsHash();
    const responsemap = new Map();
    for (const key in readFeatureDataTable) {
        if (readFeatureDataTable.hasOwnProperty(key)) {
            logger.debug(() => `Key is: ${key} and Value is: ${readFeatureDataTable[key]}`);
            responsemap.set(key, readFeatureDataTable[key]);
        }
    }
    validateJsonMapFollowedByParentIndex(parentNode, responseBody, responsemap);
}
exports.validateJsonDataTableFollowedByParentIndex = validateJsonDataTableFollowedByParentIndex;
/**
 * This method is used for validating JSON response followed by parent node index after making http request
 * Where sending input as JSON Response and DataTable as Map
 * Where DataTable is 2D array , sending row and colums as key value pair
 * Constructing Map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validateJsonMapFollowedByParentIndex: It validates map values with JSON response followed by parent node
 * followed by responseBody JSON path and parent index.
 * @param {string} parentKey - parent key
 * @param {object} responseBody - response body
 * @param {Map<string, string>} responseMap - response map
 * @returns {void}
 */
function validateJsonMapFollowedByParentIndex(parentKey, responseBody, responseMap
/*reading column values
  from feature file */ ) {
    let replacewithParentNode = "";
    let replaceNode = "";
    for (let [actualPath, expectedValue] of responseMap) {
        if (actualPath.includes(parentKey)) {
            const actualparentNode = actualPath.split(".");
            replaceNode = actualparentNode[actualparentNode.length - 2];
            replacewithParentNode = replaceJsonPathFollowedByParentIndex(responseBody, parentKey, expectedValue);
        }
        logger.debug(() => `Replace node: ${replaceNode}`);
        actualPath = actualPath.replace(replaceNode, replacewithParentNode);
        logger.debug(() => `Actual path: ${actualPath}`);
        const queryResult = jsonPath.query(responseBody, "$.." + actualPath);
        if (expectedValue.toString().includes(";")) {
            expectedValue.toString().split(";").forEach((value) => {
                logger.debug(() => `Query result value is: ${value}`);
                chai_1.assert.isTrue(queryResult.some((res) => res === value), `${actualPath} or ${value} is not present in response`);
                logger.info(() => `${actualPath} or ${value} is present in response`);
            });
        }
        else {
            chai_1.assert.strictEqual(queryResult.toString(), expectedValue.toString(), `${actualPath} or ${expectedValue} is not present in response`);
            logger.info(() => `${actualPath} or ${expectedValue} is present in response`);
        }
    }
}
exports.validateJsonMapFollowedByParentIndex = validateJsonMapFollowedByParentIndex;
/**
 * This method is used to read the JSON file from the path specified
 * @param {string} jsonfilepath - JSON file path
 */
async function readJson(jsonfilepath) {
    return load_json_file_1.default(jsonfilepath);
}
exports.readJson = readJson;
/**
 * This method is used to read the JSON file from the specified path to create a JSON object
 * @param {string} jsonfilepath - JSON file path
 */
async function readJsonToObject(jsonfilepath) {
    const jsonFileAsObject = await readJson(jsonfilepath);
    return jsonFileAsObject;
}
exports.readJsonToObject = readJsonToObject;
/**
 * This method is used to read the JSON file from the specified path to create a JSON string
 * @param {string} jsonfilepath - JSON file path
 */
async function readJsonToString(jsonfilepath) {
    const jsonFileAsObject = await readJson(jsonfilepath);
    const jsonstr = JSON.stringify(jsonFileAsObject);
    return jsonstr;
}
exports.readJsonToString = readJsonToString;
/**
 * This method is used to find the parent node by name
 * @param {object} jsonData - JSON data
 * @param {string} nodename - name of the node
 */
async function findNodeByName(jsonData, nodename) {
    const paths = jsonPath.paths(jsonData, "$.." + nodename);
    let nodes;
    for (const index in paths) {
        if (paths.hasOwnProperty(index)) {
            logger.debug(() => `Json node value is: ${jsonPath.query(jsonData, jsonPath.stringify(paths[index]).replace("$.", "$.."))}`);
            logger.debug((() => `Json path is: ${jsonPath.stringify(paths[index])}`));
            nodes.push(jsonPath.stringify(paths[index]));
        }
    }
    return nodes;
}
exports.findNodeByName = findNodeByName;
/**
 * This method is used to POST request parameters from the DataTable that are specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {TableDefinition} responseFields - reponse fields
 */
async function postRequestParametersFromDataTable(jsonfilepath, responseFields
/*reading column values
from feature file */ ) {
    const readFeatureDataTable = responseFields.rowsHash();
    const responsemap = new Map();
    for (const key in readFeatureDataTable) {
        if (readFeatureDataTable.hasOwnProperty(key)) {
            logger.debug(() => `Key is: ${key} and Value is ${readFeatureDataTable[key]}`);
            responsemap.set(key, readFeatureDataTable[key]);
        }
    }
    return postRequestParametersFromMap(jsonfilepath, responsemap);
}
exports.postRequestParametersFromDataTable = postRequestParametersFromDataTable;
/**
 * This method is used to POST request parameters from the DataTable as Map specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 */
async function postRequestParametersFromMap(jsonfilepath, dataTableFieldsInMap
/*reading column values
   from feature file */ ) {
    const jsonfileobject = await readJsonToObject(jsonfilepath);
    for (const [key, expectedValue] of dataTableFieldsInMap) {
        jsonPath.apply(jsonfileobject, "$.." + key, (value) => {
            value = expectedValue;
            logger.debug(value);
            return value;
        });
    }
    return JSON.stringify(jsonfileobject);
}
exports.postRequestParametersFromMap = postRequestParametersFromMap;
/**
 * This method is used to find the parent node value by node name
 * @param {object} jsonData - JSON data
 * @param {string} nodename - name of the node
 */
async function findNodeValue(jsonData, nodename) {
    const paths = jsonPath.paths(jsonData, "$.." + nodename);
    const arrayOfNodeValues = [];
    for (const index in paths) {
        if (paths.hasOwnProperty(index)) {
            const pathExpression = jsonPath.stringify(paths[index]).replace("$.", "$..");
            const nodeValues = jsonPath.query(jsonData, pathExpression);
            arrayOfNodeValues.push(...nodeValues);
        }
    }
    return arrayOfNodeValues;
}
exports.findNodeValue = findNodeValue;
/**
 * This method is used for validation of JSON response after making http request by giving JSON path
 * returns the table as a 2-D array, without the first row
 * where first row from datatable using it as column headers.
 * validJsonPathWithRegularExpressionForMap: It validates JSON response passed as key and value pair
 * after making service(REST) call, where key is JSON node(path) name and value is JSON node(path)
 * value with the responseBody JSON path.
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - response fields
 * @returns {void}
 */
function excludeFirstRowFromTableAndValidateJsonPath(responseBody, responseFields
/*reading column values from feature file */ ) {
    const readFeatureDataTable = responseFields.rows();
    const responsemap = readFeatureDataTable.reduce((map, v) => {
        map.set(v[0], v[1]);
        return map;
    }, new Map());
    logger.debug(() => `readFeatureDataTable :  ${JSON.stringify(responsemap)}`);
    validJsonPathWithRegularExpressionForMap(responseBody, responsemap);
}
exports.excludeFirstRowFromTableAndValidateJsonPath = excludeFirstRowFromTableAndValidateJsonPath;
/**
 * This method is used for validating JSON response followed by parent index
 * returns the data table as a 2-D array, without the first row
 * where first row from datatable using it as column headers.
 * validateJsonMapFollowedByParentIndex:It validates JSON response with Map as key and value pair
 * after making service(REST) call, where key is JSON node(path) name and value is JSON node(path) value
 * followed by responseBody JSON path and parent index.
 * @param {string} parentNode - parent node
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - response fields
 * @returns {void}
 */
function excludeFirstRowAndValidateJsonPathFollowedByParentIndex(parentNode, responseBody, responseFields /*reading column
 values from feature file */) {
    const readFeatureDataTable = responseFields.rows();
    const responsemap = readFeatureDataTable.reduce((map, v) => {
        map.set(v[0], v[1]);
        return map;
    }, new Map());
    logger.debug(() => `readFeatureDataTable :  ${JSON.stringify(responsemap)}`);
    validateJsonMapFollowedByParentIndex(parentNode, responseBody, responsemap);
}
exports.excludeFirstRowAndValidateJsonPathFollowedByParentIndex = excludeFirstRowAndValidateJsonPathFollowedByParentIndex;
/**
 * This method is used to POST request parameters from the DataTable that are specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {TableDefinition} responseFields - reponse fields
 */
async function excludeHeadersAndPostRequestParametersFromDataTable(jsonfilepath, responseFields
/*reading column values
from feature file */ ) {
    const readFeatureDataTable = responseFields.rows();
    const responsemap = readFeatureDataTable.reduce((map, v) => {
        map.set(v[0], v[1]);
        return map;
    }, new Map());
    logger.debug(() => `readFeatureDataTable :  ${JSON.stringify(responsemap)}`);
    return postRequestParametersFromMap(jsonfilepath, responsemap);
}
exports.excludeHeadersAndPostRequestParametersFromDataTable = excludeHeadersAndPostRequestParametersFromDataTable;
/**
 * This method is used to read response file in json format as well parameters from the DataTable as Map.
 * Where values of Map needs to be replaced in the input json file object
 * Where input response json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 * @returns {Promise<object>} - Returns Json object
 */
async function createJsonResponseObjectFromMap(jsonResponsefilepath, dataTableFieldsInMap
/*reading column values
   from feature file */ ) {
    const jsonfileobject = await readJsonToObject(jsonResponsefilepath);
    for (const [key, expectedValue] of dataTableFieldsInMap) {
        jsonPath.apply(jsonfileobject, "$.." + key, (value) => {
            value = expectedValue;
            logger.debug(() => `${value}`);
            return value;
        });
    }
    return jsonfileobject;
}
exports.createJsonResponseObjectFromMap = createJsonResponseObjectFromMap;
/**
 * This method is used to validate json response objects
 * These input json response file converting as json object.
 * Where internally calls deepCompareOfJsonObjects method to validate two json objects
 * @param {string} jsonfilepath - JSON Response as Input File
 * @param {object} actualJsonResponseObject - Getting json response after making REST call
 * @returns {Promise<void>} - returns nothing
 */
async function validateJsonResponseFile(jsonResponsefilepath, actualJsonResponseObject) {
    const jsonfileobject = await readJsonToObject(jsonResponsefilepath);
    deepCompareOfJsonObjects(jsonfileobject, actualJsonResponseObject);
}
exports.validateJsonResponseFile = validateJsonResponseFile;
/**
 * This method is used to POST request parameters as json file
 * These input json file converting as json object.
 * Where JsonObject converting to JSon string
 * @param {string} jsonfilepath - JSON file path
 * @returns {Promise<String>} - returns json string
 */
async function postRequestParametersAsFile(jsonfilepath) {
    const jsonfileobject = await readJsonToObject(jsonfilepath);
    return JSON.stringify(jsonfileobject);
}
exports.postRequestParametersAsFile = postRequestParametersAsFile;
/**
 * This method is used to compare of two json objects by every Key
 * Which gives exactly Key that fails for particular test
 * Where passing two json objects as inputs
 * @param {object} expectedJsonObject - First json object
 * @param {object} actualJosnObject - second json object that needs to be compared
 * @returns {<void>} - Returns Json object
 */
function deepCompareOfJsonObjects(expectedJsonObject, actualJosnObject) {
    if (Object.keys(expectedJsonObject).length === Object.keys(actualJosnObject).length) {
        iterateNestedObects(expectedJsonObject, actualJosnObject);
    }
    else {
        //Validating when expectedJsonObject keys length is less or greater than actualJosnObject keys
        chai_1.expect(JSON.stringify(expectedJsonObject)).to.be.equals(JSON.stringify(actualJosnObject));
    }
}
exports.deepCompareOfJsonObjects = deepCompareOfJsonObjects;
/**
 * This is recursive method is used to compare of two json objects
 * Mainly for looping through nested keys of complex json such as objects within the objects
 * Which gives exactly Key that fails for particular test
 * Where passing two json objects as inputs
 * @param {object} expectedJsonObject - First json object
 * @param {object} actualJosnObject - second json object that needs to be compared
 * @returns {<void>} - Returns Json object
 */
const iterateNestedObects = (expectedobj, actualobj) => {
    Object.keys(expectedobj).forEach((key) => {
        if (typeof expectedobj[key] === "object" && expectedobj[key] !== null) {
            iterateNestedObects(expectedobj[key], actualobj[key]);
        }
        else {
            logger.debug(() => `expected json parameter value: ${expectedobj[key]}  and actual json parameter value:
           ${actualobj[key]}`);
            chai_1.expect(expectedobj[key]).to.be.equals(actualobj[key], `Json parameter value not found for Key ${key}`);
        }
    });
};
/**
 * This method is used to POST request parameters from the DataTable as Map specified in the input json
 * These requested parameters will be applied on input json object.
 * Same json object will be used as input for post request
 * @param {object} jsonObject - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 */
async function postJsonObjectRequestFromMap(jsonObject, dataTableFieldsInMap
/*reading column values
   from feature file */ ) {
    for (const [key, expectedValue] of dataTableFieldsInMap) {
        jsonPath.apply(jsonObject, "$.." + key, (value) => {
            value = expectedValue;
            logger.debug(value);
            return value;
        });
    }
    return JSON.stringify(jsonObject);
}
exports.postJsonObjectRequestFromMap = postJsonObjectRequestFromMap;
//# sourceMappingURL=jsonValidationUtils.js.map