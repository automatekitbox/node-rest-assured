#!/bin/bash

SONAR_VERSION="3.2.0.1227"
FILENAME="sonar-scanner-cli-${SONAR_VERSION}-linux.zip"
OUTDIR="sonar-scanner-${SONAR_VERSION}-linux"

wget -N "https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/${FILENAME}";
unzip -o "${FILENAME}";

DEFAULT_SONAR_PARAMS="-Dsonar.host.url=$SONAR_HOST
        -Dsonar.login=$SONAR_LOGIN
        -Dsonar.password=$SONAR_PASSWORD
        -Dsonar.projectVersion=$CIRCLE_BUILD_NUM
        -Dsonar.links.homepage=$CIRCLE_REPOSITORY_URL
        -Dsonar.links.ci=$CIRCLE_BUILD_URL
        -Dsonar.links.issue=$CIRCLE_REPOSITORY_URL/issues
        -Dsonar.links.scm=$CIRCLE_REPOSITORY_URL
        -Dsonar.sourceEncoding=UTF-8"

./${OUTDIR}/bin/sonar-scanner $DEFAULT_SONAR_PARAMS
