
import {assert, expect} from "chai";
import {TableDefinition} from "cucumber";
import * as jsonPath from "jsonpath";
import {PathComponent} from "jsonpath";
import loadJsonFile from "load-json-file";

const log4js = require("log4js");
const logger = log4js.getLogger();
logger.level = "debug";

/**
 * This method is used to display JSON response in an easy-to-read format
 * @param {object} json - JSON response
 */
export function prettyPrintJSON(json: object): string {
  return JSON.stringify(json, null, " ");
}

/**
 * This method is used for the validation of JSON response after making http request.
 * Where sending input as JSON Response and DataTable
 * Constructing map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validJsonPathWithRegularExpressionForMap: It validates JSON response with DataTable values
 * @param {object} responseBody - json response body
 * @param {TableDefinition} responseFields - reading datatable from feature file
 * @returns {void}
 */
export function validateJsonPathWithRegularExpression(responseBody: object, responseFields: TableDefinition
                                                      /*reading column values from feature file */): void {
  logger.debug("Entry validateJsonPathWithRegularExpression");
  const readFeatureDataTable: { [firstCol: string]: string; } = responseFields.rowsHash();
  const responsemap: Map<string, string> = new Map();
  for (const key in readFeatureDataTable) {
    if (readFeatureDataTable.hasOwnProperty(key)) {
      logger.debug(`Key is: ${key} Value is: ${readFeatureDataTable[key]}`);
      responsemap.set(key, readFeatureDataTable[key]);
    }
  }
  validJsonPathWithRegularExpressionForMap(responseBody, responsemap);
  logger.debug("Exit validateJsonPathWithRegularExpression");
}

/**
 * This method is used for the negative validation of JSON response after making http request
 * Json response should not contain json parameter values that are passed from feature file (DataTable)
 * Where sending input as JSON Response and DataTable
 * Where DataTable is 2D array , sending row and colums as key value pair
 * This will internally refer invalidJsonPathWithRegularExpressionForMap by passing JSON path(reponseBody)
 * invalidJsonPathWithRegularExpressionForMap: It validates JSON response with DataTable values
 * @param {object} responseBody - json response
 * @param {TableDefinition} responseFields - DataTable from feature file
 * @returns {void}
 */
export function invalidJsonPathWithRegularExpression(responseBody: object, responseFields: TableDefinition): void {
  const readFeatureDataTable: { [firstCol: string]: string; } = responseFields.rowsHash();
  const responsemap: Map<string, string> = new Map();
  for (const key in readFeatureDataTable) {
    if (readFeatureDataTable.hasOwnProperty(key)) {
      logger.debug(() => `Key is: ${key} Value is: ${readFeatureDataTable[key]}`);
      responsemap.set(key, readFeatureDataTable[key]);
    }
  }
  invalidJsonPathWithRegularExpressionForMap(responseBody, responsemap);
}

/**
 * This method is used for the validation of JSON response after making http request.
 * Where sending input as JSON Response and DataTable as Map
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validJsonPathWithRegularExpressionForMap: It validates JSON response with Map values
 * @param {object} responseBody - response body
 * @param {Map<string, string>} responsemap - data table fields in Map
 * @returns {void}
 */
export function validJsonPathWithRegularExpressionForMap(responseBody: object, responsemap: Map<string, string>): void {
  for (const [actualPath, expectedValue] of responsemap) {
    const queryResult: any[] = jsonPath.query(responseBody, "$.." + actualPath);
    logger.debug(() => `Json query result is: ${queryResult}`);
    logger.debug(() => `Json expected value is json query result is: ${expectedValue}`);
    if (expectedValue.toString().includes(";")) {
      expectedValue.toString().split(";").forEach((value) => {
        assert(queryResult.some((val: any) => val === value),
            `${actualPath} or ${value} is not present in response`);
      });
    } else {
      expect(queryResult.some((x) => x.toString() === expectedValue.toString()),
          `${queryResult} is not equals to ${expectedValue} OR ${actualPath} is not present in response`)
          .to.be.true;
      logger.info(() => `${actualPath} or ${expectedValue} is present in response`);
    }
  }
}

/**
 * This method is used for the negative validation of JSON response after making http request
 * Json response should not contain json parameter(node) values that are passed from feature file (DataTable)
 * Where sending input as JSON Response and DataTable as Map
 * Where DataTable is 2D array , sending row and colums as key value pair
 * invalidJsonPathWithRegularExpressionForMap: It validates JSON response with Map values
 * @param {object} responseBody - response body
 * @param {Map<string, string>} datableFieldsInMap - data table fields in Map
 * @returns {void}
 */
export function invalidJsonPathWithRegularExpressionForMap(responseBody: object, datableFieldsInMap:
    Map<string, string>): void {
  for (const [actualPath, expectedValue] of datableFieldsInMap) {
    const queryResult: any[] = jsonPath.query(responseBody, "$.." + actualPath);
    logger.info(() => `Json query result is: ${queryResult}`);
    if (queryResult.length === 0) {
      expect(queryResult,
          `${queryResult} equals to ${expectedValue} OR ${actualPath} is present in response`)
          .to.be.empty;
      logger.info(() => `${actualPath} is not present in response`);
    } else if (queryResult.toString().includes(",")) {
      const queryResultValue: string[] = queryResult.toString().split(",");
      expect(queryResultValue.indexOf(expectedValue) === -1,
          `${expectedValue} is present in actual response ${queryResult}`).to.be.true;
      logger.info(() => `${actualPath} or ${expectedValue} is not present in response`);
    } else {
      expect(queryResult.some((x) => x.toString() !== expectedValue.toString()),
          `${queryResult} is equals to ${expectedValue} OR ${actualPath} is present in response`)
          .to.be.true;
      logger.info(() => `${actualPath} or ${expectedValue} is not present in response`);
    }
  }
}

/**
 * This method is used to get parent node JSON value by replacing JSON path with the given parent node
 * @param {object} responseBody - response body
 * @param {string} parentnode - parent node
 * @param {string} value - value
 * @returns {string} - JSON path
 */
export function replaceJsonPathFollowedByParentIndex(responseBody: object, parentnode: string, value: string): string {
  let parentNode: string = "";
  const paths: PathComponent[][] = jsonPath.paths(responseBody, "$.." + parentnode);
  for (const index in paths) {
    if (paths.hasOwnProperty(index)) {
      const jsonpath: string = jsonPath.stringify(paths[index]);
      const patharray: string[] = jsonpath.split(".");
      const jsonvalue: string = jsonPath.value(responseBody, jsonpath.replace("$", "$."));
      logger.debug(() => `Parent node Json value is: ${jsonvalue}`);
      if (jsonvalue != null && jsonvalue.toString() === value.toString()) {
        parentNode = patharray[patharray.length - 2];
        logger.debug(() => `Parent node is: ${parentNode}`);
        return parentNode;
      }
    }
  }
  return parentNode;
}

/**
 * This method is used for validating JSON response followed by parent node index after making http request
 * Where sending input as JSON Response and DataTable
 * Where DataTable is 2D array , sending row and colums as key value pair
 * Constructing Map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validateJsonMapFollowedByParentIndex: It validates map values with JSON response followed by parent node
 * followed by responseBody JSON path and parent index.
 * @param {string} parentNode - parent node
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - reponse fields
 * @returns {void}
 */
export function validateJsonDataTableFollowedByParentIndex(parentNode: string, responseBody: object,
                                                           responseFields: TableDefinition/*reading column values
                                                      from feature file */): void {
  const readFeatureDataTable: { [firstCol: string]: string; } = responseFields.rowsHash();
  const responsemap: Map<string, string> = new Map();
  for (const key in readFeatureDataTable) {
    if (readFeatureDataTable.hasOwnProperty(key)) {
      logger.debug(() => `Key is: ${key} and Value is: ${readFeatureDataTable[key]}`);
      responsemap.set(key, readFeatureDataTable[key]);
    }
  }
  validateJsonMapFollowedByParentIndex(parentNode, responseBody, responsemap);
}

/**
 * This method is used for validating JSON response followed by parent node index after making http request
 * Where sending input as JSON Response and DataTable as Map
 * Where DataTable is 2D array , sending row and colums as key value pair
 * Constructing Map out of DataTable which is defined in feature file
 * Constructed Map will contain json node and value pair
 * Json response should contain json parameter values that are passed from feature file (DataTable)
 * validateJsonMapFollowedByParentIndex: It validates map values with JSON response followed by parent node
 * followed by responseBody JSON path and parent index.
 * @param {string} parentKey - parent key
 * @param {object} responseBody - response body
 * @param {Map<string, string>} responseMap - response map
 * @returns {void}
 */
export function validateJsonMapFollowedByParentIndex(parentKey: string, responseBody: object,
                                                     responseMap: Map<string, string>
                                                     /*reading column values
                                                       from feature file */): void {
  let replacewithParentNode: string = "";
  let replaceNode: string = "";

  for (let [actualPath, expectedValue] of responseMap) {
    if (actualPath.includes(parentKey)) {
      const actualparentNode: string[] = actualPath.split(".");
      replaceNode = actualparentNode[actualparentNode.length - 2];
      replacewithParentNode = replaceJsonPathFollowedByParentIndex(responseBody, parentKey, expectedValue);
    }
    logger.debug(() => `Replace node: ${replaceNode}`);
    actualPath = actualPath.replace(replaceNode, replacewithParentNode);
    logger.debug(() => `Actual path: ${actualPath}`);
    const queryResult: any[] = jsonPath.query(responseBody, "$.." + actualPath);
    if (expectedValue.toString().includes(";")) {
      expectedValue.toString().split(";").forEach((value) => {
        logger.debug(() => `Query result value is: ${value}`);
        assert.isTrue(queryResult.some((res: any): boolean => res === value),
            `${actualPath} or ${value} is not present in response`);
        logger.info(() => `${actualPath} or ${value} is present in response`);
      });
    } else {
      assert.strictEqual(queryResult.toString(), expectedValue.toString(),
          `${actualPath} or ${expectedValue} is not present in response`);
      logger.info(() => `${actualPath} or ${expectedValue} is present in response`);
    }
  }
}

/**
 * This method is used to read the JSON file from the path specified
 * @param {string} jsonfilepath - JSON file path
 */
export async function readJson(jsonfilepath: string): Promise<object> {
  return loadJsonFile(jsonfilepath);
}

/**
 * This method is used to read the JSON file from the specified path to create a JSON object
 * @param {string} jsonfilepath - JSON file path
 */
export async function readJsonToObject(jsonfilepath: string): Promise<object> {
  const jsonFileAsObject: object = await readJson(jsonfilepath);
  return  jsonFileAsObject;
}

/**
 * This method is used to read the JSON file from the specified path to create a JSON string
 * @param {string} jsonfilepath - JSON file path
 */
export async function readJsonToString(jsonfilepath: string): Promise<string> {
  const jsonFileAsObject: object = await readJson(jsonfilepath);
  const jsonstr: string = JSON.stringify(jsonFileAsObject);
  return jsonstr;
}

/**
 * This method is used to find the parent node by name
 * @param {object} jsonData - JSON data
 * @param {string} nodename - name of the node
 */
export async function findNodeByName(jsonData: object, nodename: string): Promise<string[]> {
  const paths: PathComponent[][] =  jsonPath.paths(jsonData, "$.." + nodename);
let nodes : string[];
    for (const index in paths) {
      if (paths.hasOwnProperty(index)) {
        logger.debug(() => `Json node value is: ${jsonPath.query(jsonData,
            jsonPath.stringify(paths[index]).replace("$.", "$.."))}`);
        logger.debug((() => `Json path is: ${jsonPath.stringify(paths[index])}`));
        nodes.push(jsonPath.stringify(paths[index]));
      }
  }
  return nodes;
}

/**
 * This method is used to POST request parameters from the DataTable that are specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {TableDefinition} responseFields - reponse fields
 */
export async function postRequestParametersFromDataTable(jsonfilepath: string, responseFields: TableDefinition
                                                         /*reading column values
                                                      from feature file */): Promise<string> {
  const readFeatureDataTable: { [firstCol: string]: string; } = responseFields.rowsHash();
  const responsemap: Map<string, string> = new Map();
  for (const key in readFeatureDataTable) {
    if (readFeatureDataTable.hasOwnProperty(key)) {
      logger.debug(() => `Key is: ${key} and Value is ${readFeatureDataTable[key]}`);
      responsemap.set(key, readFeatureDataTable[key]);
    }
  }
  return postRequestParametersFromMap(jsonfilepath, responsemap);

}

/**
 * This method is used to POST request parameters from the DataTable as Map specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 */
export async function postRequestParametersFromMap(jsonfilepath: string, dataTableFieldsInMap: Map<string, string>
                                                   /*reading column values
                                                      from feature file */): Promise<string> {
  const jsonfileobject: object = await readJsonToObject(jsonfilepath);
  for (const [key, expectedValue] of dataTableFieldsInMap) {
    jsonPath.apply(jsonfileobject, "$.." + key, (value) => {
      value = expectedValue;
      logger.debug(value);
      return value;
    });
  }
  return JSON.stringify(jsonfileobject);
}

/**
 * This method is used to find the parent node value by node name
 * @param {object} jsonData - JSON data
 * @param {string} nodename - name of the node
 */
export async function findNodeValue(jsonData: object, nodename: string): Promise<string[]> {
  const paths: PathComponent[][] = jsonPath.paths(jsonData, "$.." + nodename);
  const arrayOfNodeValues: string[] = [];
  for (const index in paths) {
    if (paths.hasOwnProperty(index)) {
      const pathExpression = jsonPath.stringify(paths[index]).replace("$.", "$..");
      const nodeValues: any[] = jsonPath.query(jsonData, pathExpression);
      arrayOfNodeValues.push(...nodeValues);
    }
  }
  return arrayOfNodeValues;
}


/**
 * This method is used for validation of JSON response after making http request by giving JSON path
 * returns the table as a 2-D array, without the first row
 * where first row from datatable using it as column headers.
 * validJsonPathWithRegularExpressionForMap: It validates JSON response passed as key and value pair
 * after making service(REST) call, where key is JSON node(path) name and value is JSON node(path)
 * value with the responseBody JSON path.
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - response fields
 * @returns {void}
 */
export function excludeFirstRowFromTableAndValidateJsonPath(responseBody: object, responseFields: TableDefinition
                                                            /*reading column values from feature file */): void {
  const readFeatureDataTable: string[][] = responseFields.rows();
  const responsemap: Map<string, string> = readFeatureDataTable.reduce((map: Map<string, string>, v: any) => {
    map.set(v[0], v[1]);
    return map;
  }, new Map());
  logger.debug(() => `readFeatureDataTable :  ${JSON.stringify(responsemap)}`);
  validJsonPathWithRegularExpressionForMap(responseBody, responsemap);
}

/**
 * This method is used for validating JSON response followed by parent index
 * returns the data table as a 2-D array, without the first row
 * where first row from datatable using it as column headers.
 * validateJsonMapFollowedByParentIndex:It validates JSON response with Map as key and value pair
 * after making service(REST) call, where key is JSON node(path) name and value is JSON node(path) value
 * followed by responseBody JSON path and parent index.
 * @param {string} parentNode - parent node
 * @param {object} responseBody - response body
 * @param {TableDefinition} responseFields - response fields
 * @returns {void}
 */
export function excludeFirstRowAndValidateJsonPathFollowedByParentIndex(parentNode: string, responseBody: object,
                                                                        responseFields: TableDefinition/*reading column
                                                                         values from feature file */): void {
  const readFeatureDataTable: string[][] = responseFields.rows();
  const responsemap: Map<string, string> = readFeatureDataTable.reduce((map: Map<string, string>, v: any) => {
    map.set(v[0], v[1]);
    return map;
  }, new Map());
  logger.debug(() => `readFeatureDataTable :  ${JSON.stringify(responsemap)}`);
  validateJsonMapFollowedByParentIndex(parentNode, responseBody, responsemap);
}

/**
 * This method is used to POST request parameters from the DataTable that are specified in the input json file
 * These request parameters will replace for input json file object.
 * Where input json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {TableDefinition} responseFields - reponse fields
 */
export async function excludeHeadersAndPostRequestParametersFromDataTable(jsonfilepath: string,
                                                                          responseFields: TableDefinition
                                                                          /*reading column values
                                                                       from feature file */): Promise<string> {
  const readFeatureDataTable: string[][] = responseFields.rows();
  const responsemap: Map<string, string> = readFeatureDataTable.reduce((map: Map<string, string>, v: any) => {
    map.set(v[0], v[1]);
    return map;
  }, new Map());
  logger.debug(() => `readFeatureDataTable :  ${JSON.stringify(responsemap)}`);
  return postRequestParametersFromMap(jsonfilepath, responsemap);

}

/**
 * This method is used to read response file in json format as well parameters from the DataTable as Map.
 * Where values of Map needs to be replaced in the input json file object
 * Where input response json file just template for request parameters
 * @param {string} jsonfilepath - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 * @returns {Promise<object>} - Returns Json object
 */
export async function createJsonResponseObjectFromMap(jsonResponsefilepath: string,
                                                      dataTableFieldsInMap: Map<string, string>
                                                      /*reading column values
                                                         from feature file */): Promise<object> {
  const jsonfileobject: object = await readJsonToObject(jsonResponsefilepath);
  for (const [key, expectedValue] of dataTableFieldsInMap) {
    jsonPath.apply(jsonfileobject, "$.." + key, (value) => {
      value = expectedValue;
      logger.debug(() => `${value}`);
      return value;
    });
  }
  return jsonfileobject;
}

/**
 * This method is used to validate json response objects
 * These input json response file converting as json object.
 * Where internally calls deepCompareOfJsonObjects method to validate two json objects
 * @param {string} jsonfilepath - JSON Response as Input File
 * @param {object} actualJsonResponseObject - Getting json response after making REST call
 * @returns {Promise<void>} - returns nothing
 */
export async function validateJsonResponseFile(jsonResponsefilepath: string,
                                               actualJsonResponseObject: object): Promise<void> {
  const jsonfileobject: object = await readJsonToObject(jsonResponsefilepath);
  deepCompareOfJsonObjects(jsonfileobject, actualJsonResponseObject);
}


/**
 * This method is used to POST request parameters as json file
 * These input json file converting as json object.
 * Where JsonObject converting to JSon string
 * @param {string} jsonfilepath - JSON file path
 * @returns {Promise<String>} - returns json string
 */
export async function postRequestParametersAsFile(jsonfilepath: string): Promise<string> {
  const jsonfileobject: object = await readJsonToObject(jsonfilepath);
  return  JSON.stringify(jsonfileobject);
}


/**
 * This method is used to compare of two json objects by every Key
 * Which gives exactly Key that fails for particular test
 * Where passing two json objects as inputs
 * @param {object} expectedJsonObject - First json object
 * @param {object} actualJosnObject - second json object that needs to be compared
 * @returns {<void>} - Returns Json object
 */

export function deepCompareOfJsonObjects(expectedJsonObject: object, actualJosnObject: object): void {
  if (Object.keys(expectedJsonObject).length === Object.keys(actualJosnObject).length) {
    iterateNestedObects(expectedJsonObject, actualJosnObject);
  } else {
    //Validating when expectedJsonObject keys length is less or greater than actualJosnObject keys
    expect(JSON.stringify(expectedJsonObject)).to.be.equals(JSON.stringify(actualJosnObject));
  }
}


/**
 * This is recursive method is used to compare of two json objects
 * Mainly for looping through nested keys of complex json such as objects within the objects
 * Which gives exactly Key that fails for particular test
 * Where passing two json objects as inputs
 * @param {object} expectedJsonObject - First json object
 * @param {object} actualJosnObject - second json object that needs to be compared
 * @returns {<void>} - Returns Json object
 */

const iterateNestedObects = (expectedobj: object, actualobj: object) => {

  Object.keys(expectedobj).forEach((key) => {
    if (typeof expectedobj[key] === "object" && expectedobj[key] !== null) {
      iterateNestedObects(expectedobj[key], actualobj[key]);
    } else {
      logger.debug(() => `expected json parameter value: ${expectedobj[key]}  and actual json parameter value:
           ${actualobj[key]}`);
      expect(expectedobj[key]).to.be.equals(actualobj[key],
          `Json parameter value not found for Key ${key}`);
    }
  });
};

/**
 * This method is used to POST request parameters from the DataTable as Map specified in the input json
 * These requested parameters will be applied on input json object.
 * Same json object will be used as input for post request
 * @param {object} jsonObject - JSON file path
 * @param {Map<string, string>} dataTableFieldsInMap - data table fields Map
 */
export async function postJsonObjectRequestFromMap(jsonObject: object, dataTableFieldsInMap: Map<string, string>
                                                   /*reading column values
                                                      from feature file */): Promise<string> {
  for (const [key, expectedValue] of dataTableFieldsInMap) {
    jsonPath.apply(jsonObject, "$.." + key, (value) => {
      value = expectedValue;
      logger.debug(value);
      return value;
    });
  }
  return JSON.stringify(jsonObject);
}
