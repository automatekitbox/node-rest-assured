import * as got from "got";
// tslint:disable-next-line:no-var-requires
const FormData = require("form-data");

const form = new FormData();
let responseData: object = Object;

/*CRUD operations PUT,POST,GET,DELETE and PATCH */
export async function makeHttpRequest(url: string, headerOptions?: object,
                                      HttpMethod?: string, inputBody?: string, formFlag?: boolean): Promise<object> {

  if (headerOptions == null) {
    headerOptions = {Accept: "application/json"};
  }
  if (inputBody == null) {
    inputBody = "";
  } else if (formFlag) {
    inputBody = JSON.parse(inputBody);
  }
  if (HttpMethod == null) {
    HttpMethod = "GET"; //default GET method
  }

  if (formFlag == null) {
    formFlag = false;
  }

  try {
    responseData = await got(url, {
      headers: headerOptions,
      body: inputBody,
      form: formFlag,
      method: HttpMethod,
      retry: 1

    } as any); // Note: Using "any" for the type, since got has a number of variants for the options parameter.
               // It would be better to replace the "any" with a more specific type, if that can be determined.

  } catch (error) {
    return responseData = error.response;
  }
  return responseData;
}

/*Sending Form-Data as requestBody for CRUD operations PUT,POST,DELETE and PATCH */
export async function makeHttpRequestWithFormData(url: string, headerOptions?: object,
                                                  HttpMethod?: string,
                                                  formDataMap?: Map<string, any>): Promise<object> {
  if (headerOptions == null) {
    headerOptions = {Accept: "application/json"};
  }
  if (HttpMethod == null) {
    HttpMethod = "GET"; //default GET method
  }
  for (const [key, value] of formDataMap) {
    form.append(key, value);
  }
  try {
    responseData = await got(url, {
      headers: headerOptions,
      body: form,
      method: HttpMethod,
      retry: 1
    } as any); // Note: Using "any" for the type, since got has a number of variants for the options parameter.
               // It would be better to replace the "any" with a more specific type, if that can be determined.
  } catch (error) {
    return responseData = error.response;
  }
  return responseData;
}
