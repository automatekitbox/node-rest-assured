import { expect } from "chai";

import { prettyPrintJSON } from "../../src/common-helpers/rest-helper/jsonValidationUtils";

describe ("jsonValidationUtils", () => {
  describe("prettyPrintJSON", () => {
    it ("should return formatted JSON", async () => {
      const data: object = {
        testProp: "testValue"
      };
      const json: string = JSON.stringify(data);
      expect(json.indexOf("\n")).to.be.lessThan(0);

      const formattedJson: string = prettyPrintJSON(data);
      expect(formattedJson.indexOf("\n")).to.be.greaterThan(0);
      expect(formattedJson).to.equal("{\n \"testProp\": \"testValue\"\n}");
    });
  });
});
