# Node-Rest-Assured

It supports API automation like RestAssure , 
Extra feature ,Chai asserts supported implicitly for json response validations


## Installation

First, make sure you are logged-in to the private NPM registry:
```

```

Then install the dependency packages:
```
npm install
```

## Building the component library

First, make sure you've followed the instructions in the [Installation](installation) section so you're logged into the
private NPM registry and have installed dependency packages.


Then, to build the component library:
```
$ npm run build
```

You can then also run tests and coverage:
```
$ npm run test
$ npm run test:ci
```

## Debugging

To setup a mocha test configuration in WebStorm, do the following:

1. Open the project in WebStorm.
1. At the top right of the project window, choose _Edit Configurations..._.
1. Click the "+" symbol to add a new configuration, and choose _Mocha_.
1. Select the appropriate node interpreter. If using NVM, this should be something like, _~/.nvm/versions/node/v10.15.0/bin/node_.
1. Make sure the project root is set as the working directory.
1. Set _Extra Mocha Options_ to `--no-opts --no-config` (defensive - to ensure use of only package.json options).
1. Set _Test file patterns_ to `test-js/**/*.spec.js`.
1. Save the configuration, then run or debug.


## Publishing changes

To publish a new version to the NPM registry:



## License

Please see [LICENSE.md](LICENSE.md).
